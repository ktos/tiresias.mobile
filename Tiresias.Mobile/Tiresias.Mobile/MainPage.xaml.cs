﻿using Plugin.SimpleAudioPlayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tiresias.Mobile
{
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<string> dataCollection;
        private TcpClient tc;
        private bool end = false;
        private ISimpleAudioPlayer player;

        public MainPage()
        {
            InitializeComponent();

            dataCollection = new ObservableCollection<string>();
            data.ItemsSource = dataCollection;

            player = CrossSimpleAudioPlayer.Current;
        }

        private void BtnDisconnect_Clicked(object sender, EventArgs e)
        {
            end = true;
            tc.GetStream().Dispose();
            tc.Dispose();

            btnConnect.IsEnabled = true;
            btnDisconnect.IsEnabled = false;
        }

        private async void BtnConnect_Clicked(object sender, EventArgs e)
        {
            tc = new TcpClient();
            await tc.ConnectAsync(host.Text.Substring(0, host.Text.IndexOf(':')), int.Parse(host.Text.Substring(host.Text.IndexOf(':') + 1)));

            btnConnect.IsEnabled = false;
            btnDisconnect.IsEnabled = true;

            await Task.Run(() =>
            {
                var stream = tc.GetStream();

                end = false;
                while (!end)
                {
                    try
                    {
                        byte[] buffer = new byte[100];
                        var bytes = stream.Read(buffer, 0, buffer.Length);

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            var s = Encoding.ASCII.GetString(buffer);
                            if (s[0] != '\r')
                            {
                                if (player.IsPlaying)
                                {
                                    player.Stop();
                                }

                                player.Load($"{s[0]}.mp3");
                                player.Play();

                                dataCollection.Add(s);
                            }
                        });

                        if (bytes == 0)
                        {
                            end = true;
                            break;
                        }
                    }
                    catch (IOException)
                    {
                        end = true;
                        break;
                    }
                }
            });
        }
    }
}
